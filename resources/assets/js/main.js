var Vue = require('vue');
var VueResource = require('vue-resource')

Vue.use(VueResource);

import exampleeditor from './components/exampleeditor.vue';
import example from './components/example.vue';

 new Vue({
     el: 'body',
     data(){
         return {
             examples: {}
         }
     },
     components: {
         exampleeditor: exampleeditor,
         example: example
     },
     ready: function(){
         this.$http.get('/example/public/api/examples').then(function(response){
             this.$set('examples', response.data.data);
         });
     }
 });

