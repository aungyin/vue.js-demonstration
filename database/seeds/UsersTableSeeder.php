<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $faker = Faker::create('en_US');

        App\User::create([
            'name'  => 'Fusty',
            'email' => 'sterlingalex@gmail.com',
            'password' => bcrypt('super2'),
        ]);

        foreach(range(1,100) as $index){
            App\User::create([
                'name'  => $faker->username,
                'email' => $faker->email,
                'password' => bcrypt($faker->password),
                'created_at' => $faker->dateTimeThisYear,
                'updated_at' => $faker->dateTimeThisYear
            ]);
        }
    }
}
